//
//  AddDecorationViewController.swift
//  Kerstmaan
//
//  Created by Kris Boonefaes on 14/11/17.
//  Copyright © 2017 Appaya. All rights reserved.
//

import UIKit

class AddDecorationViewController: UIViewController {

    var targetIndex: Int?
    let colors = [UIColor.yellow, UIColor.red, UIColor.blue, UIColor.purple, UIColor.orange]
    let colorNames = ["Gele", "Rode", "Blauwe", "Paarse", "Oranje"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let treeVC = segue.destination as? TreeViewController {
            guard let selectedDecoration = sender as? IndexedDecoration else {
                return
            }
            treeVC.decorationsView.add(decoration: selectedDecoration)
        }
    }

}

extension AddDecorationViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DecorationTypes.allTypes.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return DecorationTypes.allTypes[section].rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return colors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DecorationCell", for: indexPath)
        cell.textLabel?.text = colorNames[indexPath.row] + " " + DecorationTypes.allTypes[indexPath.section].rawValue
        cell.textLabel?.textColor = colors[indexPath.row]
        return cell
    }
    
}

extension AddDecorationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let targetIndex = targetIndex else {
            return
        }
        var decoration: DecorationView?
        if DecorationTypes.allTypes[indexPath.section] == .ball {
            
        }
        let size = CGFloat(30 + arc4random() % 30)
        switch DecorationTypes.allTypes[indexPath.section] {
        case .ball:
            decoration = Ball(frame: CGRect(x: 0, y: 0, width: size, height: size), color: colors[indexPath.row])
            break
        case .star:
            decoration = Star(frame: CGRect(x: 0, y: 0, width: size + 10, height: size + 10), color: colors[indexPath.row])
            break
        }
        
        guard let selectedDecoration = decoration else {
            return
        }
        let indexedDecoration = IndexedDecoration(index: targetIndex, view: selectedDecoration)
        performSegue(withIdentifier: "unwindToTree", sender: indexedDecoration)
    }
}


