//
//  DecorationView.swift
//  Kerstmaan
//
//  Created by Kris Boonefaes on 14/11/17.
//  Copyright © 2017 Appaya. All rights reserved.
//

import UIKit

enum DecorationTypes: String {
    case ball = "bol", star = "ster"
    static let allTypes = [ball, star]
}

class DecorationView: UIView {
    
    fileprivate var color = UIColor.red.cgColor
    var callBack: (() -> ())?
    
    convenience init(frame: CGRect, color: UIColor) {
        self.init(frame: frame)
        backgroundColor = UIColor.clear
        self.color = color.cgColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapDecoration))
        addGestureRecognizer(tap)
    }
    
    @objc func didTapDecoration() {
        guard let callBack = callBack else {
            return
        }
        callBack()
    }

}

class Ball: DecorationView {
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath.init(roundedRect: rect, cornerRadius: rect.width / 2)
        path.close()
        let maskLayer = CAShapeLayer()
        maskLayer.fillColor = color
        maskLayer.path = path.cgPath
        
        layer.addSublayer(maskLayer)
        clipsToBounds = true
    }
    
}

class Empty: DecorationView {
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath.init(roundedRect: rect, cornerRadius: rect.width / 2)
        path.close()
        let maskLayer = CAShapeLayer()
        maskLayer.lineWidth = 2
        maskLayer.fillColor = UIColor.white.cgColor
        maskLayer.strokeColor = UIColor.white.cgColor
        maskLayer.path = path.cgPath
        
        layer.addSublayer(maskLayer)
        clipsToBounds = true
    }
    
}

class Star: DecorationView {
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        let starExtrusion:CGFloat = rect.width / 4
        let center = CGPoint(x: rect.midX, y: rect.midY)
        let points = 7
        var angle:CGFloat = -CGFloat(.pi / 2.0)
        let angleIncrement = CGFloat(.pi * 2.0 / Double(points))
        let radius = rect.midX
        
        var firstPoint = true
        
        for _ in 1...points {
            
            let point = pointFrom(angle: angle, radius: radius, offset: center)
            let nextPoint = pointFrom(angle: angle + angleIncrement, radius: radius, offset: center)
            let midPoint = pointFrom(angle: angle + angleIncrement / 2.0, radius: starExtrusion, offset: center)
            
            if firstPoint {
                firstPoint = false
                path.move(to: point)
            }
            
            path.addLine(to: midPoint)
            path.addLine(to: nextPoint)
            
            angle += angleIncrement
        }
        
        path.close()
        let maskLayer = CAShapeLayer()
        maskLayer.fillColor = color
        maskLayer.path = path.cgPath
        
        layer.addSublayer(maskLayer)
        clipsToBounds = true
    }
    
    func pointFrom(angle: CGFloat, radius: CGFloat, offset: CGPoint) -> CGPoint {
        return CGPoint(x: radius * cos(angle) + offset.x, y: radius * sin(angle) + offset.y)
    }
    
}


