//
//  TreeView.swift
//  Kerstmaan
//
//  Created by Kris Boonefaes on 14/11/17.
//  Copyright © 2017 Appaya. All rights reserved.
//

import UIKit

class TreeView: UIView {

    var branchCount: CGFloat = 4
    var decorationPoints = [CGPoint]()
    
    override func layoutSubviews() {
        drawTree(in: frame, with: branchCount)
    }
    
    func drawTree(in rect: CGRect, with branches: CGFloat) {
        let moonHeight = rect.height / (branches + 4)
        let trunkHeight = moonHeight / 2
        let branchesHeight = rect.height - moonHeight - trunkHeight
        
        let branchesAndPoints = drawBranches(in: CGRect(x: 0, y: moonHeight, width: rect.width, height: branchesHeight), with: branches)
        let branchesLayer = CAShapeLayer()
        branchesLayer.path = branchesAndPoints.0.cgPath
        branchesLayer.fillColor = UIColor(red: 0, green: 0.75, blue: 0.0, alpha: 1.0).cgColor
        branchesLayer.strokeColor = UIColor(red: 0, green: 0.5, blue: 0.0, alpha: 1.0).cgColor
        branchesLayer.lineWidth = 2
        layer.addSublayer(branchesLayer)
        decorationPoints = branchesAndPoints.1
        let trunk = drawTrunk(in: CGRect(x: rect.width / 2 - (trunkHeight / 2), y: rect.height - trunkHeight, width: trunkHeight, height: trunkHeight))
        let trunkLayer = CAShapeLayer()
        trunkLayer.path = trunk.cgPath
        trunkLayer.fillColor = UIColor.brown.cgColor
        layer.addSublayer(trunkLayer)
        let moon = drawMoon(in: CGRect(x: rect.width / 2 - (moonHeight / 2), y: 0, width: moonHeight, height: moonHeight))
        let moonLayer = CAShapeLayer()
        moonLayer.path = moon.cgPath
        moonLayer.fillColor = UIColor.yellow.cgColor
        moonLayer.strokeColor = UIColor.orange.cgColor
        moonLayer.lineWidth = 2
        layer.addSublayer(moonLayer)
    }
    
    func drawBranches(in rect: CGRect, with branches: CGFloat) -> (UIBezierPath, [CGPoint]) {
        let branchesPath = UIBezierPath()
        var branchesPoints = [CGPoint]()
        let branchHeight = rect.height / branches
        var branchTop: CGFloat = rect.minY
        var branchWidth = rect.width / branches
        var branch: CGFloat = 1
        while branch <= branches {
            branchWidth = rect.width * (branch / branches)
            let branchRect = CGRect(x: rect.midX - branchWidth / 2, y: branchTop, width: branchWidth, height: branchHeight)
            branchesPoints.append(contentsOf: [CGPoint(x: branchRect.maxX - branchWidth, y: branchRect.maxY),
                                               CGPoint(x: branchRect.midX, y: branchRect.midY),
                                               CGPoint(x: branchRect.midX, y: branchRect.minY + branchHeight),
                                               CGPoint(x: branchRect.maxX, y: branchRect.maxY)])
            branchesPath.append(drawFlattenedTriangle(in: branchRect, with: branch == 1 ? 0 : branchWidth / 5))
            branch += 1
            branchTop += branchHeight
        }
        return (branchesPath, branchesPoints)
    }
    
    func drawMoon(in rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.midX, y: rect.maxY))
        path.addQuadCurve(to: CGPoint(x: rect.midX, y: rect.minY), controlPoint: CGPoint(x: rect.minX - rect.width / 1.5, y: rect.midY))
        path.addQuadCurve(to: CGPoint(x: rect.midX, y: rect.maxY), controlPoint: CGPoint(x: rect.minX, y: rect.midY))
        path.close()
        return path
    }
    
    func drawTrunk(in rect: CGRect) -> UIBezierPath {
        let trunkPath = drawSquare(in: rect)
        UIColor.brown.setFill()
        trunkPath.fill()
        return trunkPath
    }
    
    func drawSquare(in rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.close()
        return path
    }
    
    
    func drawFlattenedTriangle(in rect: CGRect, with inset: CGFloat) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.midX - inset, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.midX + inset, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.close()
        return path
    }
    
}
