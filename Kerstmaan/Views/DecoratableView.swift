//
//  DecoratableView.swift
//  Kerstmaan
//
//  Created by Kris Boonefaes on 14/11/17.
//  Copyright © 2017 Appaya. All rights reserved.
//

import UIKit

class DecoratableView: UIView {

    private var points = [CGPoint]()
    private var didSetup = false
    var decorations = [IndexedDecoration]()
    var delegate: DecorationDelegate?
    
    func setup(with points: [CGPoint]) {
        if didSetup {
            return
        }
        didSetup = true
        self.points = points
        for (index, point) in points.enumerated() {
            if decorations.count < index {
                
            } else {
                let placeHolder = Empty(frame: CGRect(origin: CGPoint(x: point.x, y: point.y), size: CGSize(width: 25, height: 25)), color: UIColor.yellow)
                placeHolder.center = point
                let decoration = IndexedDecoration(index: index, view: placeHolder)
                decorations.append(decoration)
            }
            
        }
        drawDecorations()
    }
    
    func add(decoration: IndexedDecoration) {
        decorations[decoration.index] = decoration
        drawDecorations()
    }
    
    func drawDecorations() {
        subviews.forEach({ $0.removeFromSuperview() })

        for decoration in decorations {
            decoration.view.callBack = {
                guard let delegate = self.delegate else {
                    return
                }
                delegate.didTapDecoration(at: decoration.index)
            }
            let point = points[decoration.index]
            decoration.view.center = point
            addSubview(decoration.view)
        }
    }

}

protocol DecorationDelegate {
    
    func didTapDecoration(at index: Int)
    
}

struct IndexedDecoration {
    var index: Int
    var view: DecorationView
}
