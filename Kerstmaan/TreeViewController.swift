//
//  ViewController.swift
//  Kerstmaan
//
//  Created by Kris Boonefaes on 14/11/17.
//  Copyright © 2017 Appaya. All rights reserved.
//

import UIKit

class TreeViewController: UIViewController {

    @IBOutlet weak var treeView: TreeView!
    @IBOutlet weak var decorationsView: DecoratableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        decorationsView.setup(with: treeView.decorationPoints)
        decorationsView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindTotree(segue: UIStoryboardSegue) {
        decorationsView.drawDecorations()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addVC = segue.destination as? AddDecorationViewController {
            guard let index = sender as? Int else {
                return
            }
            addVC.targetIndex = index
        }
    }
}

extension TreeViewController: DecorationDelegate {
    
    func didTapDecoration(at index: Int) {
        self .performSegue(withIdentifier: "showDecorationsPicker", sender: index)
    }
    
}
